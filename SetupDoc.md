We'll make partially use of John Wise's setup script to get initial nested grids
with must-refine-particles
~~~
hg clone https://bitbucket.org/jwise77/enzo-mrp-music rg128
cd rg128
cp template.conf wrapperrL0.conf
~~~

First, we run a unigrid dark-matte only simulation at 512^3 to identify the most massive halo.
Thus, we increase the root grid size in template.conf and set a seed at the appropriate level.
~~~
--- template.conf	2017-05-12 17:51:16.793310285 -0400
+++ wrapper-L0.conf	2017-05-12 17:50:48.343263494 -0400
@@ -1,9 +1,9 @@
 [setup]
 boxlength 	= 10
 zstart		= 100
-levelmin	= 5
-levelmin_TF	= 5
-levelmax	= 5
+levelmin	= 9
+levelmin_TF	= 9
+levelmax	= 9
 padding		= 2
 overlap		= 0
 ref_center	= 0.5, 0.5, 0.5
@@ -23,10 +23,7 @@
 transfer	= eisenstein  ##this cannot be changed yet!
 
 [random]
-seed[5]		= 201510130
-seed[6]		= 301510130
-seed[7]		= 401510130
-seed[8]		= 501510130
+seed[9]		= 201705120
 
 [output]
 ##generic FROLIC data format (used for testing)
~~~

and run MUSIC for the first time
`./MUSIC wrapper-L0.conf 2>&1 |tee music-L0.log`


Next, the dark matter only simulation.
~~~
cd wrapper-L0
cp parameter_file.txt wrapper-L0.enzo
~~~

Edit/add to parameter_file.txt
~~~
CosmologyFinalRedshift                   = 15

CosmologyOutputRedshift[0] = 100.
CosmologyOutputRedshift[1] = 15.0
~~~

Run enzo
`<enzo.exe> -d wrapper-L0.enzo 2>&1 |tee enzo.out`

Find halo of interest (here most massive one)

`use yt's to find most massive halo`

Enter the information to the script that identifies the Lagrangrian region in the first snapshot (see bottom of get_halo_initial_extent.py)
~~~
edit get_halo_initial_extent.py
python get_halo_inital_extent.py
~~~

That should give you a file with the initial particle positions.
Now, we rerun MUSIC with the target root grid size, i.e. reduce levelmin to 7, in a new file `cp wrapper-L0.conf wrapper-L2.conf`
However, no new seeds should be set. The seed from the highest level is automatically coarsened to the lower resolution grids.
Also set baryons to yes and add refinement based on the particle position file (see MUSIC manual).
Rerun MUSIC.
Finally, rerun Enzo with the additional parameters for hydro, refinement etc. (see the parameter file in the other repo I sent a few days ago).
